package Repository1Java;


import java.util.Scanner;
public class Main {

    public static float doDivision(int num1, int num2) throws ArithmeticException{


        return num1/num2;
    }


    public static void main(String[] args) {

        int tryAgain = 0;


        while (tryAgain == 0) {


        Scanner x = new Scanner(System.in);
        System.out.println("First Number");
        int num1 = x.nextInt();

        Scanner y = new Scanner(System.in);
        System.out.println("Second Number");
        int num2 = y.nextInt();


        float result = 0;


        try {
            result = doDivision(num1, num2);
        } catch (ArithmeticException exc) {
            System.out.println("Cannot divide by 0. Please input a different number.");
        }

        if (result != 0)
        {
            tryAgain = 1;
            System.out.println("The result is "+ result);
        }

    }

    }
}
