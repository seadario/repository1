package Repository1Java;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        // Set -- A grocery List where users can add their products
        System.out.println("---Grocery List---");

        Set set = new TreeSet();
        set.add("Apples");
        set.add("Lettuce");
        set.add("Milk");
        set.add("Honey");
        set.add("Carrots");
        for (Object groceries : set) {
            System.out.println((String) groceries);
        }
        // List -- Marbles that were taken out of a bag
        System.out.println("---Marbles in bag---");
        List list = new ArrayList();
        list.add("Red marble");
        list.add("Blue marble");
        list.add("Red marble");
        list.add("Yellow marble");
        list.add("Green marble");
        list.add("Blue marble");

        for (Object marbles : list) {
            System.out.println((String) marbles);


        }
        //Queue -- An alphabetical role call for students

        System.out.println("---Roll Call---");
        Queue queue = new PriorityQueue();
        queue.add("John");
        queue.add("Simon");
        queue.add("Zack");
        queue.add("Deborah");
        queue.add("Jeremiah");
        queue.add("Stanley");
        queue.add("Caitlyn");
        queue.add("Kennedy");


        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());

        }

        // Map -- A customer's order, from a restaurant menu
        System.out.println("---Restaurant Order---");
        Map map = new HashMap();
        map.put(1,"Hamburger");
        map.put(2,"CheeseBurger");
        map.put(3,"Fries");
        map.put(4,"Drink");
        map.put(5,"Cookies");
        map.put(6,"Hot Dog");
        map.put(7,"Condiments");
        map.put(8,"Salad");
        map.put(9,"Ice Cream");

        System.out.println(map.get(2));
        System.out.println(map.get(6));
        System.out.println(map.get(4));
        System.out.println(map.get(9));

        // List using Generics --

        System.out.println("---Generics---");
        List<Vehicles> myList = new LinkedList<Vehicles>();
        myList.add(new Vehicles("Sedan|","Mercedes|","Grey"));
        myList.add(new Vehicles("Sedan|","Nissan|","Red"));
        myList.add(new Vehicles("Convertible|","Chevrolet|","Blue"));
        myList.add(new Vehicles("SUV|","Hyundai|","White"));

        for (Vehicles print : myList) {
            System.out.println(print);
        }
    }
}