package Repository1Java;

public class Vehicles {

    private String type;
    private String model;
    private String color;

    public Vehicles (String type, String model, String color)
    {
        this.type = type;
        this.model = model;
        this.color = color;
    }

    public String toString(){
        return "Type: " + type + " Model: " + model + " Color: " + color;
    }

}
